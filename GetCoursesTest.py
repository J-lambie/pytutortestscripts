import os
import unittest
import tempfile
from app import app, db, forms, models
from sqlalchemy import text


class GetCourseTestCase(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

    def tearDown(self):
        db.session.remove()

    def test_courses_inserted_by_create_all_script(self):
        # Fails until you import the SQL file...
        full_course_list = [('', '-- Choose a Course --'), (u'COMP1125', u'COMP1125'), (u'COMP1126', u'COMP1126'), (u'COMP1127', u'COMP1127'), (u'COMP1160', u'COMP1160'), (u'COMP1161', u'COMP1161'), (u'COMP1210', u'COMP1210'), (u'COMP1220', u'COMP1220'), (u'COMP2120', u'COMP2120'), (u'COMP2130', u'COMP2130'), (u'COMP2140', u'COMP2140'), (u'COMP2170', u'COMP2170'), (u'COMP2180', u'COMP2180'), (u'COMP2190', u'COMP2190'), (u'COMP2201', u'COMP2201'), (u'COMP2211', u'COMP2211'), (u'COMP2230', u'COMP2230'), (u'COMP2340', u'COMP2340'), (u'COMP3101', u'COMP3101'), (u'COMP3150', u'COMP3150'), (u'COMP3161', u'COMP3161'), (u'COMP3170', u'COMP3170'), (u'COMP3180', u'COMP3180'), (u'COMP3191', u'COMP3191'), (u'COMP3192', u'COMP3192'), (u'COMP3220', u'COMP3220'), (u'COMP3651', u'COMP3651'), (u'COMP3652', u'COMP3652'), (u'COMP3702', u'COMP3702'), (u'COMP3801', u'COMP3801'), (u'COMP3901', u'COMP3901'), (u'COMP3911', u'COMP3911'), (u'COMP3912', u'COMP3912'), (u'COMP5110', u'COMP5110'), (u'COMP5120', u'COMP5120'), (u'COMP5710', u'COMP5710'), (u'COMP5730', u'COMP5730'), (u'COMP5740', u'COMP5740'), (u'COMP5770', u'COMP5770'), (u'COMP6010', u'COMP6010'), (u'COMP6105', u'COMP6105'), (u'COMP6110', u'COMP6110'), (u'COMP6420', u'COMP6420'), (u'COMP6430', u'COMP6430'), (u'COMP6550', u'COMP6550'), (u'COMP6770', u'COMP6770'), (u'COMP6900', u'COMP6900'), (u'INFO2100', u'INFO2100'), (u'INFO2110', u'INFO2110'), (u'INFO2180', u'INFO2180'), (u'INFO3105', u'INFO3105'), (u'INFO3110', u'INFO3110'), (u'INFO3110', u'INFO3110'), (u'INFO3155', u'INFO3155'), (u'INFO3155', u'INFO3155'), (u'INFO3170', u'INFO3170'), (u'INFO3180', u'INFO3180'), (u'INFO3435', u'INFO3435'), (u'SWEN3130', u'SWEN3130'), (u'SWEN3145', u'SWEN3145'), (u'SWEN3165', u'SWEN3165'), (u'SWEN3185', u'SWEN3185'), (u'SWEN3920', u'SWEN3920')]
        self.assertEquals(
            forms.getCourses(),
            full_course_list
        )

    def test_gets_courses_after_a_course_is_aded(self):
        new_full_course_list = [('', '-- Choose a Course --'), (u'COMP1125', u'COMP1125'), (u'COMP1126', u'COMP1126'), (u'COMP1127', u'COMP1127'), (u'COMP1160', u'COMP1160'), (u'COMP1161', u'COMP1161'), (u'COMP1210', u'COMP1210'), (u'COMP1220', u'COMP1220'), (u'COMP2120', u'COMP2120'), (u'COMP2130', u'COMP2130'), (u'COMP2140', u'COMP2140'), (u'COMP2170', u'COMP2170'), (u'COMP2180', u'COMP2180'), (u'COMP2190', u'COMP2190'), (u'COMP2201', u'COMP2201'), (u'COMP2211', u'COMP2211'), (u'COMP2230', u'COMP2230'), (u'COMP2340', u'COMP2340'), (u'COMP3101', u'COMP3101'), (u'COMP3150', u'COMP3150'), (u'COMP3161', u'COMP3161'), (u'COMP3170', u'COMP3170'), (u'COMP3180', u'COMP3180'), (u'COMP3191', u'COMP3191'), (u'COMP3192', u'COMP3192'), (u'COMP3220', u'COMP3220'), (u'COMP3651', u'COMP3651'), (u'COMP3652', u'COMP3652'), (u'COMP3702', u'COMP3702'), (u'COMP3801', u'COMP3801'), (u'COMP3901', u'COMP3901'), (u'COMP3911', u'COMP3911'), (u'COMP3912', u'COMP3912'), (u'COMP5110', u'COMP5110'), (u'COMP5120', u'COMP5120'), (u'COMP5710', u'COMP5710'), (u'COMP5730', u'COMP5730'), (u'COMP5740', u'COMP5740'), (u'COMP5770', u'COMP5770'), (u'COMP6010', u'COMP6010'), (u'COMP6105', u'COMP6105'), (u'COMP6110', u'COMP6110'), (u'COMP6420', u'COMP6420'), (u'COMP6430', u'COMP6430'), (u'COMP6550', u'COMP6550'), (u'COMP6770', u'COMP6770'), (u'COMP6900', u'COMP6900'), (u'INFO2100', u'INFO2100'), (u'INFO2110', u'INFO2110'), (u'INFO2180', u'INFO2180'), (u'INFO3105', u'INFO3105'), (u'INFO3110', u'INFO3110'), (u'INFO3110', u'INFO3110'), (u'INFO3155', u'INFO3155'), (u'INFO3155', u'INFO3155'), (u'INFO3170', u'INFO3170'), (u'INFO3180', u'INFO3180'), (u'INFO3435', u'INFO3435'), (u'otherCode', u'otherCode'), (u'SWEN3130', u'SWEN3130'), (u'SWEN3145', u'SWEN3145'), (u'SWEN3165', u'SWEN3165'), (u'SWEN3185', u'SWEN3185'), (u'SWEN3920', u'SWEN3920')]
        c = models.Course(CourseCode='Test1234', CourseName='TestCourse', OtherCode='otherCode')

        db.session.add(c)

        self.assertEquals(
            forms.getCourses(),
            new_full_course_list
        )

    def test_gets_courses_after_deleted_course(self):
        c = models.Course.query.filter_by(OtherCode='SWEN3920').first()

        db.session.delete(c)

        new_course_list = [('', '-- Choose a Course --'), (u'COMP1125', u'COMP1125'), (u'COMP1126', u'COMP1126'), (u'COMP1127', u'COMP1127'), (u'COMP1160', u'COMP1160'), (u'COMP1161', u'COMP1161'), (u'COMP1210', u'COMP1210'), (u'COMP1220', u'COMP1220'), (u'COMP2120', u'COMP2120'), (u'COMP2130', u'COMP2130'), (u'COMP2140', u'COMP2140'), (u'COMP2170', u'COMP2170'), (u'COMP2180', u'COMP2180'), (u'COMP2190', u'COMP2190'), (u'COMP2201', u'COMP2201'), (u'COMP2211', u'COMP2211'), (u'COMP2230', u'COMP2230'), (u'COMP2340', u'COMP2340'), (u'COMP3101', u'COMP3101'), (u'COMP3150', u'COMP3150'), (u'COMP3161', u'COMP3161'), (u'COMP3170', u'COMP3170'), (u'COMP3180', u'COMP3180'), (u'COMP3191', u'COMP3191'), (u'COMP3192', u'COMP3192'), (u'COMP3220', u'COMP3220'), (u'COMP3651', u'COMP3651'), (u'COMP3652', u'COMP3652'), (u'COMP3702', u'COMP3702'), (u'COMP3801', u'COMP3801'), (u'COMP3901', u'COMP3901'), (u'COMP3911', u'COMP3911'), (u'COMP3912', u'COMP3912'), (u'COMP5110', u'COMP5110'), (u'COMP5120', u'COMP5120'), (u'COMP5710', u'COMP5710'), (u'COMP5730', u'COMP5730'), (u'COMP5740', u'COMP5740'), (u'COMP5770', u'COMP5770'), (u'COMP6010', u'COMP6010'), (u'COMP6105', u'COMP6105'), (u'COMP6110', u'COMP6110'), (u'COMP6420', u'COMP6420'), (u'COMP6430', u'COMP6430'), (u'COMP6550', u'COMP6550'), (u'COMP6770', u'COMP6770'), (u'COMP6900', u'COMP6900'), (u'INFO2100', u'INFO2100'), (u'INFO2110', u'INFO2110'), (u'INFO2180', u'INFO2180'), (u'INFO3105', u'INFO3105'), (u'INFO3110', u'INFO3110'), (u'INFO3110', u'INFO3110'), (u'INFO3155', u'INFO3155'), (u'INFO3155', u'INFO3155'), (u'INFO3170', u'INFO3170'), (u'INFO3180', u'INFO3180'), (u'INFO3435', u'INFO3435'), (u'SWEN3130', u'SWEN3130'), (u'SWEN3145', u'SWEN3145'), (u'SWEN3165', u'SWEN3165'), (u'SWEN3185', u'SWEN3185')]

        self.assertEquals(
            forms.getCourses(),
            new_course_list
        )

    def test_gets_empty_list_with_empty_db(self):

        courses = models.Course.query.all()

        for course in courses:
            db.session.delete(course)

        empty_course_list = [("", "-- Choose a Course --")]
        self.assertEqual(
            forms.getCourses(),
            empty_course_list
        )

if __name__ == '__main__':
    unittest.main()