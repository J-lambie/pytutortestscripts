import os
import unittest
from flask.ext.testing import TestCase
from mock import MagicMock
import tempfile
from app import app , db, models
from app.models import User
from werkzeug import check_password_hash, generate_password_hash

def init_database():
        test1=User(firstname="Test1",lastname="Dummy1",email="testvalue1@test.com",password=generate_password_hash("right password"),status=1)
        test2=User(firstname="Test2",lastname="Dummy2",email="testvalue2@test.com",password=generate_password_hash("right password"),status=0)
        db.session.add(test1)
        db.session.commit()
        db.session.add(test2)
        db.session.commit()

class LoginTestCase(unittest.TestCase):

    render_templates = False
    
    def setUp(self):
        self.db_fd, app.config['DATABASE'] = tempfile.mkstemp()
        self.app = app.test_client()
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        db.create_all()
        init_database()
        

    def tearDown(self):
        os.close(self.db_fd)
        db.session.remove()
        db.drop_all()
    
    def login(self,email,password,remember_me):
        return self.app.post('/',data=dict(email=email,password=password,remember_me=remember_me),follow_redirects=True)
    
    def logout(self):
        return self.app.get('/logout', follow_redirects=True)
        
    def test_login_get(self):
        rv=self.app.get('/')
        self.assertIn('Log In',rv.data)
        
    def test_login_post_1(self):
        #not a user test
        rv = self.login("notuser@gmail.com", "wrong password",False)
        self.assertIn('The username or password you entered is incorrect!', rv.data)
        
    def test_login_post_2(self):  
        #correct user but incorrect password and active
        rv=self.login("testvalue1@test.com","wrong password",False)
        self.assertIn('Please check your username and password!', rv.data)
        
    def test_login_post_3(self):   
        #user but blank password
        rv=self.login("testvalue1@test.com","",False)
        self.assertIn('The username or password you entered is incorrect!', rv.data)
    
    def test_login_post_4(self):  
        #correct user, correct password, no remember me and is active
        try:
            rv = self.login("testvalue1@test.com", "right password",False)
            self.assertIn('You have successfully logged in', rv.data)
            self.logout()
        except Exception as inst:
            self.assertEqual(type(inst),None)
            
    def test_login_post_5(self):  
        #correct user correct, password, remember me and is active
        try:
            rv = self.login("testvalue1@test.com", "right password",True)
            self.assertIn( 'You have successfully logged in',rv.data)
            self.logout()
        except Exception as inst:
            self.assertEqual(type(inst),None)
    
    def test_login_post_6(self):
        #correct user, correct password but not active
        rv = self.login("testvalue2@test.com", "right password",False)
        self.assertIn("Your account needs activation!", rv.data)
        self.logout()
    
    def test_login_post_7(self):   
        #correct user, wrong password and not active
        rv = self.login("testvalue2@test.com", "wrong password",False)
        self.assertIn("Your account needs activation!", rv.data)
        
        
if __name__ == '__main__':
    unittest.main()