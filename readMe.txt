============================================================
-               READ ME                                    -
============================================================

-Each Unit Test Cases are named after Unit they are testing
-In order to run the scripts you will need:
    1.the application to be in the app folder
    2.Install the required libraries and frameworks
        -Using a virtual environment and then pip install 
         the requirements. Commands: 
            virtualenv venv
            source venv/bin/activate
            pip install -r requirements.txt
            
         -or just installing the requirements using pip 
          install. Command:
            pip install -r requirements.txt
            
    3.the mysql database to be up
        -by starting mysql
        -creating the database called "TimeTable"
        -running the sql script called TimeTable.sql
    
    4.run any of the scripts.  Command:
        python ScriptName.py
        
#Important!!!!!
-Ensure to run the msql file before running any of the 
 scripts