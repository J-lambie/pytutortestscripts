import tempfile
import unittest
import datetime

from wtforms.validators import Required

from app import app, db, forms
from app.models import Marking


class MarkingFormTestCase(unittest.TestCase):
    def setUp(self):
        self.db_fd, app.config['DATABASE'] = tempfile.mkstemp()
        self.app = app.test_client()
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        db.create_all()

    def tearDown(self):
        db.session.remove()

    # A user has to be logged in to use the 'Add Marker' feature
    def login(self, email, password, remember_me):
        return self.app.post('/', data=dict(email=email, password=password, remember_me=remember_me),
                             follow_redirects=True)

    # cc - CourseCode
    # ui - UserInitial
    # si - SemesterId
    # ac - AssignmentCode
    # ad - AssignmentDue
    # md - MarkingDue
    # sn - SubmissionNumber
    # gr - GradingRate
    def markingForm(self, cc, ui, si, ac, ad, md, sn, gr):
        ccSelectField = forms.SelectField('CourseCode', choices=cc, default=2, validators=[Required()])
        uiSelectField = forms.SelectField('UserInitial', choices=ui, default=1, validators=[Required()])
        siSelectField = forms.SelectField('SemesterId', choices=si, default=2, validators=[Required()])

        assert type(ac) == str
        return self.app.post('/addMarker', data=dict(CourseCode=ccSelectField, userInitial=uiSelectField, SemesterId=siSelectField,
                                                     AssignmentCode=ac, AssignmentDue=ad, MarkingDue=md,
                                                     SubmissionNumber=sn, GradingRate=gr))

    def testMarkingFormGet(self):
        self.login('admin@gmail.com', 'password123', False)
        rv = self.app.get('/addMarker')
        assert 'Add Marker' in rv.data

    def testMarkingAddedToDB(self):
        marking = Marking(Id = 11, CourseCode = "COMP1161", userInitial = "TUTOR", SemesterId = "1", AssignmentCode = "3", AssignmentDue = "2016-05-04 00:00:00", MarkingDue = "2016-05-25 00:00:00", SubmissionNumber = 85, GradingRate = 50)
        db.session.add(marking)
        db.session.commit()
        assert marking in db.session()

    def testMarkingFormPostNotLoggedIn(self):
        rv = self.markingForm(forms.COURSES, forms.INITIALSTUTOR, forms.SEM, '1', '2016-05-04 00:00:00',
                              '2016-05-04 00:00:00', '85', '50')
        self.assertIn('Redirecting', rv.data)

    def testMarkingFormPostWithValidData(self):
        self.login('admin@gmail.com', 'password123', False)
        rv = self.markingForm(forms.COURSES, forms.INITIALSTUTOR, forms.SEM, '1', '2016-05-04 00:00:00', '2016-05-25 00:00:00', '85', '50')
        self.assertIn('Marker Added', rv.data)

    def testMarkingFormInvalidData(self):
        self.login('admin@gmail.com', 'password123', False)
        rv = self.markingForm(None, forms.INITIALS, forms.SEM, '1', '2016-05-04', '2016-05-25', '85', '50')
        self.assertIn('Not a valid datetime value', rv.data)

    def testMarkingFormUnfilledData(self):
        self.login('admin@gmail.com', 'password123', False)
        rv = self.markingForm(None, forms.INITIALS, forms.SEM, '1', '2016-05-04 00:00:00', '2016-05-25 00:00:00', None, '50')
        self.assertIn('This field is required', rv.data)


if __name__ == '__main__':
    unittest.main()
