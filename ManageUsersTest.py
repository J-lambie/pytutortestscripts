import os,sys
import unittest
import tempfile
from flask.ext.testing import TestCase
from app.models import User
from app import app, db, models
from flask.ext.login import current_user
from flask import request

current_user = User

#Setup database
def init_database():
    test1 = User(email = "admin1@gmail.com", firstname = "ad", lastname = "min1", password = "admin1", status = 1, role = 1)
    test2 = User(email = "admin2@gmail.com", firstname = "ad", lastname = "min2", password = "admin2", status = 0, role = 1)
    test3 = User(email = "user1@gmail.com", firstname = "us", lastname = "er1", password = "user1", status = 1, role = 0)
    test4 = User(email = "user2@gmail.com", firstname = "us", lastname = "er2", password = "user2", status = 0, role = 0)
    db.session.add(test1)
    db.session.commit()
    db.session.add(test2)
    db.session.commit()
    db.session.add(test3)
    db.session.commit()
    db.session.add(test4)
    db.session.commit()
    
#Testing class    
class FlaskTestCase(unittest.TestCase):
    
    render_templates = False
    #Setup of each testcase
    def setUp(self):
        self.db_fd, app.config['DATABASE'] = tempfile.mkstemp()
        self.app = app.test_client()
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        db.create_all()
        init_database()
        
    #Removal of each testcase    
    def tearDown(self):
        os.close(self.db_fd)
        db.session.remove()
        db.drop_all()
    
    # Login function   
    def login (self, email, password, remember_me):
        return self.app.post('/login', data=dict(email=email,password = password, remember_me=remember_me), follow_redirects=True)
    
    #Logout function    
    def logout (self):
        return self.app.get('/logout', follow_redirects=True)
        
    #Manage function    
    def manageget(self):
        return self.app.get('/manageusers', follow_redirects=True)
        
    def managepost(self):
        return self.app.post('/managers', data=dict(), follow_redirects=True)
    
        
        
    # Ensure flask was set up properly
    def test_index(self):
        response = self.app.get('/', content_type='html/text')
        self.assertEqual(response.status_code,200)
    
    # Ensure that manageusers requires login
    def test_manageusers_requires_login(self):
        response = self.manageget()
        self.assertIn('Please Log In', response.data)
    

    #Ensure loggedin admin user view all users
    def test_loggedin_admin(self):
        response = self.login("admin1@gmail.com","admin1",True)
        self.assertTrue(current_user.is_active()== True)
        self.assertTrue(current_user.role == True)
        response = self.managepost()
        self.assertIn('All Users', response.data)
        self.logout()
    
    #Ensure logged out admin user redirected to login page
    def test_loggedout_admin(self):
        self.login("admin2@gmail.com", "admin2", False)
        self.logout()
        self.assertFalse(current_user.status == True)
        self.assertFalse(current_user.role ==True)
        response = self.manageget()
        self.assertIn('Please Log In',response.data)
    
    #Ensure logged in regular user redirected to 404 page
    def test_loggedin_reguser(self):
        response = self.login("user1@gmail.com","user1", True)
        self.assertTrue(current_user.is_active == True)
        self.assertFalse(current_user.is_admin == True)
        response = self.managepost()
        self.assertIn('The administrator has been notified. Sorry for the inconvenience!', response.data)
    
    
    #Ensure logged out regular user redirected to login page
    def test_loggedout_reguser(self):
        self.login("user2@gmail.com", "user2", False)
        self.assertFalse(current_user.is_admin == True)
        self.logout()
        self.assertFalse(current_user.is_active == True)
        response = self.manageget()
        self.assertIn(b'Please Log In',response.data)
            

    #Ensure that all users are recieved in manageusers
    def test_users_recv(self):
        user = User(email = "user3@gmail.com", firstname = "us", lastname = "er3", password = "user3", status = 0, role = 0)
        db.session.add(user)
        db.session.commit()
        assert user in db.session()
   
if __name__ == '__main__':
    unittest.main()
        